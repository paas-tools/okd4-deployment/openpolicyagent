# Open Policy Agent

This project contains the code to use [OPA](https://www.openpolicyagent.org) to implement the functionality described in [here](https://its.cern.ch/jira/projects/CIPAAS/issues/CIPAAS-474).

## Deployment overview

This chart allows deployment of multiple instances of OPA in each Openshift environment, with a separate instance for each
cluster component deployed by okd4-install that requires OPA.
Each instance is deployed into the component-specific namespace (**we support no more than one OPA instance per namespace**).

The chart takes care an array rego rules as input (see `values.yaml`) and creates configmaps from them; and then configures the
[`kube-mgmt`](https://github.com/open-policy-agent/kube-mgmt) agent of OPA to load rego rules from these configmaps.
See below for more details about how OPA works with Openshift.

We use `cert-manager` to provision certificates for the OPA automatically (required for Kubernetes web hooks).

### Updating OPA

We deploy the official chart published as GitHub Pages as per <https://github.com/open-policy-agent/kube-mgmt/pull/79>.
The source and version of the chart being installed are controlled by Helm values `mgmt.chartRepoURL` and `mgmt.chartVersion`.

The version of the upstream chart is synched with the version of the [`kube-mgmt`](https://github.com/open-policy-agent/kube-mgmt) agent
but isn't updated every time a new OPA release comes out.
The version of OPA deployed by the chart is controlled by Helm values `opa.imageTag` (OPA itself). Find the container image in [DockerHub](https://hub.docker.com/r/openpolicyagent/opa/tags/) e.g. [openpolicyagent/opa:0.70.0](https://hub.docker.com/layers/openpolicyagent/opa/0.70.0/images/sha256-2b1c4b231f6dfa6a2baea4af4109203b10cd4b65e836d72f03bfe5d8dfce7095?context=explore).

We typically want to bump both versions: OPA and kube-mgmt when there's a new release.

New releases can be found in:
- <https://github.com/open-policy-agent/opa/releases> for OPA itself. E.g. release `v0.36.1` => set Helm value `opa.imageTag: 0.36.1`
- <https://github.com/open-policy-agent/kube-mgmt/releases> for `kube-mgmt`. E.g. release `3.2.1` => set Helm value `mgmt.chartVersion: 3.2.1`


## Deployment and Updates

See <https://okd-internal.docs.cern.ch/shared-sub-components/openpolicyagent/> for how this is used in the okd4-install
umbrella Helm chart.
This page also contains instructions for updating OPA and the kube-mgmt chart.

## How does OPA work with Openshift

OPA interacts with Openshift through [custom admission controllers](https://docs.okd.io/3.11/architecture/additional_concepts/dynamic_admission_controllers.htm), there is an [article](https://blog.openshift.com/fine-grained-policy-enforcement-in-openshift-with-open-policy-agent/) in the Openshift blog worth reading as an introduction to the topic. However, notice that this deployment is more inline to what is done in the OPA documentation for the [Kubernetes Admission Control](https://www.openpolicyagent.org/docs/kubernetes-admission-control.html).

This project deploys a mutating webhook that is triggered every time an action is performed on a resource. The webhook will then send a request to the OPA service to validate the request against the policies inside OPA. Once OPA receives the request it will query its policies and then reply to the request. For a more detailed explanation read the guide on [Kubernetes Admission Control flow](https://www.openpolicyagent.org/docs/latest/guides-kubernetes-admission-control/) (different from the one linked previously) in the OPA website.

Policies and information about the system are uploaded to OPA through [kube-mgmt](https://github.com/open-policy-agent/kube-mgmt).
kube-mgmt is used for:

- Loading policies into OPA via Kubernetes (see Policies in the link above.)
  - Policies are loaded into OPA using ConfigMaps.
- Replicating Kubernetes resources including CustomResourceDefinitions (CRDs) into OPA (see Caching in the link above.)

## Adding new policies

The chart takes care an array rego rules as input (see `values.yaml`) and creates configmaps from them; and then configures the
[`kube-mgmt`](https://github.com/open-policy-agent/kube-mgmt) agent of OPA to load rego rules from these configmaps.

### 1. Writing policies

Follow the example in (old) rule file <https://gitlab.cern.ch/paas-tools/okd4-deployment/openpolicyagent/-/blob/964368343ef53f3ba047b58231ea884053b5b19b/chart/templates/route-tn-access.yaml>

Writing a policy in rego is similar to writing a YACC file for a compiler where you have to think what objects do you want to match.

For instance, let's consider the problem of the route access described in [here](https://its.cern.ch/jira/projects/CIPAAS/issues/CIPAAS-474). First, we want to reject all routes that don't have the label ```router.cern.ch/technical-network-allowed``` in the namespace hence, we need to first specify that we are targeting request of kind ```Route``` where the operation is to either ```CREATE``` or ```UPDATE``` we do this by writing the following:

```rego
operations = ["CREATE", "UPDATE"]
...
input.request.kind.kind = "Route"
operations[_] = input.request.operation
```

After we first want to make sure we are dealing with a request to the technical network and since the annotation on the route can have ```True``` written in multiple ways we first transform the value inside the annotation ```router.cern.ch/technical-network-access``` to lower case and then we compare with ```true```

```rego
lower(input.request.object.metadata.annotations["router.cern.ch/technical-network-access"], label)
label == "true"
```

Finally, now that we know we are considering a request to access the technical network we want to make sure that the project has the desirable label and reject it in case it doesn't and return an error message, we do this by writing:

```rego
not namespaces[input.request.namespace].metadata.labels["router.cern.ch/technical-network-allowed"]
msg = sprintf("Project namespace %q is not whitelisted to access technical network", [input.request.namespace])
```

Now we will need to create a rule to validate the values in the label and another rule for the case where the project has the label ```router.cern.ch/technical-network-allowed``` set to ```Intranet``` and the route has the annotation ```router.cern.ch/network-visibility``` set to ```Internet```.

For more information on writing policies, it is advised to go through the example in [tutorial](https://www.openpolicyagent.org/docs/kubernetes-admission-control.html) for kubernetes admission control and also the ["How Do I Write Policies?"](https://www.openpolicyagent.org/docs/how-do-i-write-policies.html) in the OPA documentation.

### 2. Configurating the admissionControllerRules

If the resource (which the policy applies to) is not already in the list in the field ```opa.admissionControllerRules.resources``` in the file ```chart/values.yaml```, just add it like the example:

```yaml
  admissionControllerRules:
      ...
      resources: ["routes", (new resource)]
```

### 3. Configuring the resources being replicated

`data.kubernetes` contains only the Kubernetes that are "replicated" as OPA data by OPA's `kube-mgmt`. There is no direct access
to the Kubernetes cluster's "live" resources, only the replicated resources. Therefore each resource we want to access under
`data.kubernetes` must be explicitly replicated.

Helm value `opa.mgmtReplicate` indicates the resources to replicate.
If the resource you want is not being replicated you **must to add it** to the list accordingly.

```yaml
opa:
  mgmt:
    ...
    replicate:
      cluster:
        - "v1/namespaces"
        (insert new resource to be replicated at cluster level)
      namespace:
        (insert new resource to be replicated at namespace level)
```

We auto-generate RBAC so `kube-mgmt` has permissions to watch and replicate these resources.

### 4. Deploying and validating policies

The Helm deployment automatically updates policies. Deployment will succeed even if a rego policy is invalid.

Verify that rego policies are valid by looking at annotation `openpolicyagent.org/policy-status` on the `configmaps`
created for each rego rule by this chart (`rule-name.rego`). Annotation will have value `{"status":"ok"}` if rule
is valid, or show some error message otherwise.


### 5. Debugging policies

Use the Rego playground https://play.openpolicyagent.org/ and select some `Kubernetes` examples.

See [the upstream debugging tips](https://www.openpolicyagent.org/docs/latest/guides-kubernetes-admission-control/#debugging-tips)
for Kubernetes/Openshift

Useful for interactive debugging: `oc edit deploy/opa -n <component namespace>` and set `replicas: 1`,
`loglevel=debug` and remove liveness/readinessprobes.
