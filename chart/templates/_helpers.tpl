
{{/*
Definition needed to allow us to dynamically gnerate RBAC rules for the mgmt
component. Thanks to https://github.com/open-policy-agent/kube-mgmt#caching we know that
the every resource string on the array mgmt.resources will at least be composed by
version/resourcePlural with the other version being apiGroup/version/resourcePlural
then by simply spliting by / we can get each part interesting to us.
*/}}
{{- define "mgmt.resource" -}}
{{- $parts := split "/" . -}}
{{- if eq (len $parts) 2 -}}
{{- $parts._1 -}}
{{- else -}}
{{- $parts._2 -}}
{{- end -}}
{{- end -}}

{{/* See comment for definition mgmt.resource */}}
{{- define "mgmt.apiGroup" -}}
{{- $parts := split "/" . -}}
{{- if eq (len $parts) 2 -}}
{{- "" | quote -}}
{{- else -}}
{{- $parts._0 -}}
{{- end -}}
{{- end -}}